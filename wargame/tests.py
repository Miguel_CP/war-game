from unittest import TestCase

from wargame.models import *


class TestWarGame(TestCase):

    def setUp(self):
        self.knight = Knight()
        self.archer = Archer()
        self.catapult = Catapult()
        self.army1 = Army([self.knight, self.archer])
        self.army2 = Army([self.catapult])

    def test_units_initialization_stats(self):
        self.assertEqual(self.knight.health, 100)
        self.assertEqual(self.knight.damage, 20)
        self.assertEqual(self.archer.health, 50)
        self.assertEqual(self.archer.damage, 25)
        self.assertEqual(self.catapult.health, 200)
        self.assertEqual(self.catapult.damage, 50)

    def test_unit_cannot_attack_if_it_is_not_alive(self):
        unit = Unit(50, 10)
        unit.is_alive = False
        unit.attack_unit(self.catapult)
        self.assertFalse(unit.is_alive)
        self.assertEqual(self.catapult.health, 200)

    def test_unit_can_attack_check_another_unit_loses_life(self):
        attacker = Unit(100, 100)
        attacked = Unit(100, 100)
        attacker.attack_unit(attacked)
        self.assertEqual(attacked.health, 0)
        self.assertFalse(attacked.is_alive)

    def test_not_being_able_to_equip_a_weapon_to_a_catapult(self):
        with self.assertRaises(Exception):
            self.catapult.equip_weapon(Weapon(20))

    def test_knights_can_equip_a_weapon_check_that_the_damage_adds_up(self):
        knight = Knight()
        sword = Weapon(20)
        attacked = Unit(100, 100)
        knight.equip_weapon(sword)
        knight.attack_unit(attacked)
        self.assertEqual(attacked.health, 60)

    def test_archers_can_equip_a_weapon_check_that_the_damage_adds_up(self):
        archer = Archer()
        arch = Weapon(50)
        attacked = Unit(100, 100)
        archer.equip_weapon(arch)
        archer.attack_unit(attacked)
        self.assertEqual(attacked.health, 25)

    def test_create_an_army_with_units_check_sum_of_life_and_damage(self):
        army = Army([self.knight, self.archer, self.catapult])
        self.assertEqual(army.health(), 350)
        self.assertEqual(army.damage(), 95)

    def test_cannot_create_an_army_without_units(self):
        with self.assertRaises(TypeError):
            Army()
        with self.assertRaises(Exception):
            Army([])
            Army([1,2,3])


    def test_adding_units_to_the_army_check_that_are_added(self):
        self.army1.add_unit(Knight())
        self.army1.add_unit(Archer())
        self.army1.add_unit(Catapult())
        self.assertEqual(self.army1.health(), 500)
        self.assertEqual(self.army1.damage(), 140)

    def test_adding_army_to_the_army_check_that_are_added(self):
        self.army2.add_army(self.army1)
        self.assertEqual(self.army2.health(), 350)
        self.assertEqual(self.army2.damage(), 95)

    def test_the_damage_of_the_army_is_the_sum_of_the_damage_of_its_living_units(self):
        self.army1.units[0].is_alive = False
        self.assertEqual(self.army1.damage(), 25)

    def test_an_army_can_attack_a_unit_check_that_the_life_of_the_unit_has_decreased(self):
        attacked = Unit(100, 100)
        self.army1.attack_unit(attacked)
        self.assertEqual(attacked.health, 55)

    def test_an_army_cannot_attack_an_army_check_that_the_life_of_the_army_has_not_decreased(self):
        army_health = self.army2.health()
        self.army1.attack_unit(self.army2)
        self.assertEqual(self.army2.health(), army_health)

    def test_unit_cannot_attack_army_check_that_the_life_of_the_army_has_not_decreased(self):
        unit = Unit(100, 10)
        army_health = self.army1.health()
        unit.attack_unit(self.army1)
        self.assertEqual(army_health, self.army1.health())
