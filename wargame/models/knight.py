from wargame.models.unit import Unit


class Knight(Unit):

    def __init__(self):
        super().__init__(health=100, damage=20)
