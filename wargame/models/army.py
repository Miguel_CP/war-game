from wargame.models.unit import Unit


class Army:

    def __init__(self, units):
        self.validate_units(units)
        self.units = units

    def health(self):
        health = 0
        for unit in self.units:
            health += unit.health

        return health

    def damage(self):
        damage = 0
        for unit in self.units:
            if unit.is_alive:
                damage += unit.damage

        return damage
    
    def add_unit(self, unit):
        if isinstance(unit, Unit):
            self.units.append(unit)

    def add_army(self, army):
        if isinstance(army, Army):
            self.units.extend(army.units)

    def attack_unit(self, unit):
        if isinstance(unit, Unit):
            unit.receive_damage(self.damage())

    def validate_units(self, units):
        valid = True
        if len(units) > 0:
            for unit in units:
                if not isinstance(unit, Unit):
                    valid = False
                    break
        else:
            valid = False

        if valid is False:
            raise Exception("It is only allowed to create armies with units.")
