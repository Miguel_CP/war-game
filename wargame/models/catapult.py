from wargame.models.unit import Unit


class Catapult(Unit):

    def __init__(self):
        super().__init__(health=200, damage=50)

    def equip_weapon(self, weapon):
        raise Exception("catapults cannot be equipped with weapons")
