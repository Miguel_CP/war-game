from wargame.models.unit import Unit


class Archer(Unit):

    def __init__(self):
        super().__init__(health=50, damage=25)
