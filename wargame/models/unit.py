class Unit:

    def __init__(self, health: int, damage: int):
        self.health = health
        self.damage = damage
        self.is_alive = True
        self.weapon = None

    def total_damage(self):
        total_damage = self.damage
        if self.weapon:
            total_damage += self.weapon.damage

        return total_damage

    def attack_unit(self, unit):
        if isinstance(unit, Unit) and self.is_alive:
            unit.receive_damage(self.total_damage())

    def receive_damage(self, damage):
        self.health -= damage
        if self.health <= 0:
            self.is_alive = False

    def equip_weapon(self, weapon):
        self.weapon = weapon
