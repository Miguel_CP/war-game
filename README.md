# War Game

## Enunciado
El ejercicio consiste en modelar y crear los tests correspondientes a un juego de guerra, que cumpla los siguientes requisitos:

## Notas
No hace falta hacer la persistencia en una base de datos ni los endpoints, simplemente hacer los tests que prueben los casos y las clases de cada objeto que pienses que es necesario.

## Requisitos
- [x] Se deben poder inicializar y usar 3 tipos distintos de unidades: caballeros, arqueras y catapultas.
- [x] La unidades tienen puntos de vida y daño de ataque que ejercen sobre otras unidades:
  - Caballero: 20 de daño de ataque y 100 de vida.
  - Arquera: 25 de daño de ataque y 50 de vida.
  - Catapulta: 50 de daño de ataque y 200 de vida.

- [x] Si una unidad se queda sin puntos de vida, entonces está muerta y no puede atacar.
- [x] Cualquier unidad debe poder atacar a cualquier otra unidad.
- [x] Los caballeros y arqueras pueden ser equipados con armas que tienen un daño de ataque que se adiciona al daño de ataque de la unidad; las catapultas no pueden ser equipadas con armas (El sistema debe impedirlo).
EJ: Un caballero ejerce 20 puntos de daño de ataque, si se equipa con una espada cuyo daño de ataque sea 10, entonces el daño total ejercido es de 30 puntos.
- [x] Se deben poder generar ejércitos entre cualquier tipo de unidad y entre ejército mismos, es decir, que a un ejército se puede adherir una tropa u otro ejército. 
Ej: un ejército  puede ser un caballero y una arquera; también un ejército puede ser un caballero y otro ejército.
- [x] Una unidad dentro de un ejército ejerce daño de ataque solo si está viva.
- [x] Los ejércitos pueden atacar a cualquier unidad.
- [x] Los ejércitos no pueden ser atacados.

## Acerca del Ejercicio
- Se debe crear un repositorio de git (usar Gitlab, Github o BitBucket) y subir la solución en un proyecto de Django.
- No hace falta almacenar en base de datos la información, con la suite de tests es suficiente.
- No hace falta implementar interfaces, ni endpoints.

## Ejecución del ejercicio
El ejercicio ha sido desarrollado en un entorno con:
- Python 3.10.6
- Django 4.1.7

Se recomienda usar dichas versiones para asegurarse el correcto funcionamiento, así como **git** para poder clonar el proyecto.

Teniendo Python, Django y git instalado seguir los siguientes pasos:
1. clonar el repositorio
    ```
    git clone https://gitlab.com/Miguel_CP/war-game.git
    ```
2. posicionarse dentro del repositorio generado:
    ```
    cd war-game
    ```
3. ejecutar la suite de tests:
    ```
    python3 manage.py test
    ```

